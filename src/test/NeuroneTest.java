package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.Neurone;

public class NeuroneTest {
	@BeforeEach
	protected void setUp() throws Exception {

	}

	@Test
	public void testCalcul() throws Exception {
		Neurone input = new Neurone(1);
		Neurone calc = new Neurone();
		calc.addEntrée(input, 1);
		calc.addEntrée(input, (float) 0.25);
		calc.addEntrée(input, (float) 0.5);
		calc.addEntrée(input, (float) 0.25);
		assertEquals(1, Math.round(calc.activationResult()));
		ArrayList<Double> poids = new ArrayList<Double>();
		poids.add(-3.);
		poids.add(-0.001);
		poids.add(0.001);
		poids.add(-0.1);
		calc.setPoids(poids);
		assertEquals(0, Math.round(calc.activationResult()));

		Neurone n1 = new Neurone();
		n1.addEntrée(input, 1);
		n1.addEntrée(input, 1);
		n1.addEntrée(input, 1);
		Neurone n2 = new Neurone();
		n2.addEntrée(input, 1);
		n2.addEntrée(input, 1);
		n2.addEntrée(input, 1);

		Neurone n3 = new Neurone();
		n3.addEntrée(n1, 1);
		n3.addEntrée(n2, 1);
		n1.activationResult();
		n2.activationResult();

		System.out.println(n3.activationResult());
	}

	@Test
	public void testInitineuroneDernier() throws Exception {
		// throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testInitineuronePremier() throws Exception {
		// throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testInitNeurone() throws Exception {
		// throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testNeurone() throws Exception {
		// throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testNeuroneDouble() throws Exception {
		// throw new RuntimeException("not yet implemented");
	}

	@Test
	public void testRetropropagation() throws Exception {
		// throw new RuntimeException("not yet implemented");
	}

}
