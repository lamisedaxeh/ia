package test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.security.InvalidAlgorithmParameterException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.ReadImage;

public class ReadImageTest {
	ReadImage image;

	@BeforeEach
	protected void setUp() throws Exception {
		try {
			image = new ReadImage("/home/lamisedaxeh/git/ia/output96.jpg");
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetArray() throws Exception {
		assertFalse(image.getArray().isEmpty());
	}

	@Test
	public void testGetOnePixel() throws Exception {
		assertEquals(27, image.getOnePixel(0, 0));
	}

}
