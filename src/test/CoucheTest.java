package test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.Couche;

public class CoucheTest {

	@BeforeEach
	protected void setUp() throws Exception {
	}

	@Test
	public final void testAjoutDeNeurones() throws Exception {
		Couche c1 = new Couche(3);
		Couche c2 = new Couche(2);
		Couche c3 = new Couche(1);
		c1.getNeurones().get(0).setValue(1);
		c1.getNeurones().get(1).setValue(1);
		c1.getNeurones().get(2).setValue(1);

		c2.linkEntrées(c1);
		c3.linkEntrées(c2);

		c2.getNeurones().get(0).activationResult();
		c2.getNeurones().get(1).activationResult();

		System.out.println(c3.getNeurones().get(0).activationResult());

	}

	@Test
	public final void testCouche() throws Exception {
		// TODO
		//throw new RuntimeException("not yet implemented");
	}

	@Test
	public final void testCoucheInt() throws Exception {
		// TODO
		//throw new RuntimeException("not yet implemented");
	}

}
