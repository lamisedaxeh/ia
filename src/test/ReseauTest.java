package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.security.InvalidAlgorithmParameterException;
import java.util.ArrayList;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.Algo;
import fr.lamisedaxeh.Couche;
import fr.lamisedaxeh.ReadImage;
import fr.lamisedaxeh.Reseau;

public class ReseauTest {

	@Test
	public final void atestApprentisageMultiCouche() throws Exception {
		System.out.println("==> TEST APPRENTISSAGE Multi Couche");
		int row = 100;
		double[][] sick = new double[row][2];
		double[][] healthy = new double[row][2];

		Random ra = new Random();
		for (int i = 0; i < row; i++) {
			sick[i][0] = (ra.nextGaussian() + 3);
			sick[i][1] = (ra.nextGaussian() - 3);

			healthy[i][0] = (ra.nextGaussian() + 3);
			healthy[i][1] = (ra.nextGaussian() + 3);

		}
		// for (int i = row / 2; i < row; i++) {
		// sick[i][0] = (ra.nextGaussian() - 3);
		// sick[i][1] = (ra.nextGaussian() + 3);
		// healthy[i][0] = (ra.nextGaussian() - 3);
		// healthy[i][1] = (ra.nextGaussian() - 3);
		// }
		// Numbers of row per class

		ArrayList<ArrayList<Double>> features = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> targets = new ArrayList<ArrayList<Double>>();
		int ivar;
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> f = new ArrayList<Double>();
			f.add(sick[ivar][0]);
			f.add(sick[ivar][1]);
			features.add(f);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> f = new ArrayList<Double>();
			f.add(healthy[ivar][0]);
			f.add(healthy[ivar][1]);
			features.add(f);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> t = new ArrayList<Double>();
			t.add(0.);
			targets.add(t);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> t = new ArrayList<Double>();
			t.add(1.);
			targets.add(t);
		}

		Reseau r = new Reseau();
		r.addCouche(2);
		r.addCouche(2);
		r.addCouche(2);
		r.addCouche(1);

		Couche c0 = r.getCouches().get(0);
		Couche c1 = r.getCouches().get(1);
		Couche c2 = r.getCouches().get(2);
		Couche c3 = r.getCouches().get(3);
		// c1.linkEntrées(c0);
		// c2.linkEntrées(c1);
		c3.linkEntrées(c0);
		//c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());
		//c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());

		// System.out.println(c1.getNeurones().get(0));
		r.apprentisage(features, targets);

		// System.out.println(c1.getNeurones().get(0));

		assertEquals(80, Math.round(Algo.urgenceAAugmenterP((float) 0.6, 1) * 100));
	}

	@BeforeEach
	protected void setUp() throws Exception {
		System.out.println("=== TEST RESEAU ===");
	}

	@Test
	public final void testAddCouche() throws Exception {
		Reseau r = new Reseau();
		r.addCouche(3);
		r.addCouche(2);
		r.addCouche(1);

		Couche c1 = r.getCouches().get(0);
		Couche c2 = r.getCouches().get(1);
		Couche c3 = r.getCouches().get(2);

		c1.getNeurones().get(0).setValue(1);
		c1.getNeurones().get(1).setValue(1);
		c1.getNeurones().get(2).setValue(1);

		c2.linkEntrées(c1);
		c3.linkEntrées(c2);

		r.calculs();

		System.out.println(c3.getNeurones().get(0).getValue());
	}

	@Test
	public final void testApprentisage() throws Exception {
		System.out.println("==> TEST APPRENTISSAGE P");
		int row = 100;
		double[][] sick = new double[row][2];
		double[][] healthy = new double[row][2];

		Random ra = new Random();
		for (int i = 0; i < row; i++) {
			sick[i][0] = (ra.nextGaussian() - 3);
			sick[i][1] = (ra.nextGaussian() - 3);
			healthy[i][0] = (ra.nextGaussian() + 3);
			healthy[i][1] = (ra.nextGaussian() + 3);
		}
		// Numbers of row per class

		ArrayList<ArrayList<Double>> features = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> targets = new ArrayList<ArrayList<Double>>();
		int ivar;
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> f = new ArrayList<Double>();
			f.add(sick[ivar][0]);
			f.add(sick[ivar][1]);
			features.add(f);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> f = new ArrayList<Double>();
			f.add(healthy[ivar][0]);
			f.add(healthy[ivar][1]);
			features.add(f);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> t = new ArrayList<Double>();
			t.add(0.);
			targets.add(t);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> t = new ArrayList<Double>();
			t.add(1.);
			targets.add(t);
		}

		Reseau r = new Reseau();
		r.addCouche(2);
		r.addCouche(1);

		Couche c0 = r.getCouches().get(0);
		Couche c1 = r.getCouches().get(1);
		c1.linkEntrées(c0);
		c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());
		c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());

		// System.out.println(c1.getNeurones().get(0));
		r.apprentisage(features, targets);

		// System.out.println(c1.getNeurones().get(0));

		assertEquals(80, Math.round(Algo.urgenceAAugmenterP((float) 0.6, 1) * 100));
	}

	@Test
	public final void testApprentisageAvecImage() throws Exception {
		//SETUP RESEAU
		Reseau r = new Reseau();
		r.addCouche(2500);
		r.addCouche(150);
		r.addCouche(50);
		r.addCouche(10);
		r.addCouche(4);



		ArrayList<ArrayList<Double>> features = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> targets = new ArrayList<ArrayList<Double>>();

		File folder = new File("/home/lamisedaxeh/git/ia/shapes/square/100");
		File[] listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()) {

				ReadImage image = new ReadImage(file.getPath());
				ArrayList<Double> pixel = image.getArray();

				if (pixel.size() != 2500) {
					throw new InvalidAlgorithmParameterException();
				}
				ArrayList<Double> d = new ArrayList<Double>();

				for (int i = 0; i < 2500; i++) {
					d.add(pixel.remove(0));
				}

				features.add(d);
				ArrayList<Double> result = new ArrayList<Double>();
				result.add(1.);
				result.add(0.);
				result.add(0.);
				result.add(0.);
				targets.add(result);
			}
		}

		folder = new File("/home/lamisedaxeh/git/ia/shapes/circle/100");
		listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()) {

				ReadImage image = new ReadImage(file.getPath());
				ArrayList<Double> pixel = image.getArray();

				if (pixel.size() != 2500) {
					throw new InvalidAlgorithmParameterException();
				}
				ArrayList<Double> d = new ArrayList<Double>();

				for (int i = 0; i < 2500; i++) {
					d.add(pixel.remove(0));
				}

				features.add(d);
				ArrayList<Double> result = new ArrayList<Double>();
				result.add(0.);
				result.add(1.);
				result.add(0.);
				result.add(0.);
				targets.add(result);
			}
		}

		folder = new File("/home/lamisedaxeh/git/ia/shapes/star/100");
		listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()) {

				ReadImage image = new ReadImage(file.getPath());
				ArrayList<Double> pixel = image.getArray();

				if (pixel.size() != 2500) {
					throw new InvalidAlgorithmParameterException();
				}
				ArrayList<Double> d = new ArrayList<Double>();

				for (int i = 0; i < 2500; i++) {
					d.add(pixel.remove(0));
				}

				features.add(d);
				ArrayList<Double> result = new ArrayList<Double>();
				result.add(0.);
				result.add(0.);
				result.add(1.);
				result.add(0.);
				targets.add(result);
			}
		}

		folder = new File("/home/lamisedaxeh/git/ia/shapes/triangle/100");
		listOfFiles = folder.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile()) {

				ReadImage image = new ReadImage(file.getPath());
				ArrayList<Double> pixel = image.getArray();

				if (pixel.size() != 2500) {
					throw new InvalidAlgorithmParameterException();
				}
				ArrayList<Double> d = new ArrayList<Double>();

				for (int i = 0; i < 2500; i++) {
					d.add(pixel.remove(0));
				}

				features.add(d);
				ArrayList<Double> result = new ArrayList<Double>();
				result.add(0.);
				result.add(0.);
				result.add(0.);
				result.add(1.);
				targets.add(result);
			}
		}

		Couche c0 = r.getCouches().get(0);
		Couche c1 = r.getCouches().get(1);
		Couche c2 = r.getCouches().get(2);
		Couche c3 = r.getCouches().get(3);
		Couche c4 = r.getCouches().get(4);

		c1.linkEntrées(c0);
		c2.linkEntrées(c1);
		c3.linkEntrées(c2);
		c4.linkEntrées(c3);
		// c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());
		// c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());

		// System.out.println(c1.getNeurones().get(0));
		r.apprentisage(features, targets);

	}

	@Test
	public final void testAvecImage() throws Exception {
		System.out.println("==> Début testAvecImage()");
		long startTime = System.nanoTime();

		Reseau r = new Reseau();
		r.addCouche(2500);
		r.addCouche(100);
		r.addCouche(10);
		r.addCouche(10);
		r.addCouche(1);

		Couche c1 = r.getCouches().get(0);
		Couche c2 = r.getCouches().get(1);
		Couche c3 = r.getCouches().get(2);
		Couche c4 = r.getCouches().get(3);
		Couche c5 = r.getCouches().get(4);



		c2.linkEntrées(c1);
		c3.linkEntrées(c2);
		c4.linkEntrées(c3);
		c5.linkEntrées(c4);
		System.out.println((System.nanoTime() - startTime) / 1000000000 + "Graph Setup");

		c1.setInput("/home/lamisedaxeh/git/ia/1.png");
		System.out.println((System.nanoTime() - startTime) / 1000000000 + "Image Setup");
		r.calculs();
		System.out.println((System.nanoTime() - startTime) / 1000000000 + "Calc 1 ended");

		c1.setInput("/home/lamisedaxeh/git/ia/1.png");
		System.out.println((System.nanoTime() - startTime) / 1000000000 + "Image Setup");
		r.calculs();
		System.out.println((System.nanoTime() - startTime) / 1000000000 + "Calc 1 ended");

		System.out.println(c5.getNeurones().get(0).getValue());
	}
}
