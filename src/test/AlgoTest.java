package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.lamisedaxeh.Algo;
import fr.lamisedaxeh.Couche;
import fr.lamisedaxeh.Reseau;

public class AlgoTest {

	@BeforeEach
	protected void setUp() throws Exception {
		System.out.println("=== TEST ALGO ===");
	}

	@Test
	public final void testApprentisage() throws Exception {
		System.out.println("==> TEST APPRENTISSAGE P");

		double[][] sick = new double[100][2];
		double[][] healthy = new double[100][2];
		int row = 100;
		Random ra = new Random();
		for (int i = 0; i < row; i++) {
			sick[i][0] = (ra.nextGaussian() - 2);
			sick[i][1] = (ra.nextGaussian() - 2);
			healthy[i][0] = (ra.nextGaussian() + 2);
			healthy[i][1] = (ra.nextGaussian() + 2);
		}
		// Numbers of row per class

		ArrayList<ArrayList<Double>> features = new ArrayList<ArrayList<Double>>();
		ArrayList<Double> targets = new ArrayList<Double>();
		int ivar;
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> f = new ArrayList<Double>();
			f.add(sick[ivar][0]);
			f.add(sick[ivar][1]);
			features.add(f);
		}
		for (ivar = 0; ivar < row; ivar++) {
			ArrayList<Double> f = new ArrayList<Double>();
			f.add(healthy[ivar][0]);
			f.add(healthy[ivar][1]);
			features.add(f);
		}
		for (ivar = 0; ivar < row; ivar++) {
			targets.add(0.);
		}
		for (ivar = 0; ivar < row; ivar++) {
			targets.add(1.);
		}

		Reseau r = new Reseau();
		r.addCouche(2);
		r.addCouche(1);

		Couche c0 = r.getCouches().get(0);
		Couche c1 = r.getCouches().get(1);
		c1.linkEntrées(c0);
		c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());
		c1.getNeurones().get(0).getPoids().set(0, ra.nextGaussian());

		// System.out.println(c1.getNeurones().get(0));
		Algo.apprentisage(c1.getNeurones().get(0), features, targets);

		// System.out.println(c1.getNeurones().get(0));

		assertEquals(80, Math.round(Algo.urgenceAAugmenterP((float) 0.6, 1) * 100));
	}

	@Test
	public final void testTauxErreur() throws Exception {
		System.out.println("==> TEST TAUX ERREUR");

		assertEquals(16, Math.round(Algo.tauxErreur((float) 0.6, 1) * 100));
	}

	@Test
	public final void testUrgenceAAugmenter() throws Exception {
		System.out.println("==> TEST URGENCE A AUGMENTER P");

		assertEquals(80, Math.round(Algo.urgenceAAugmenterP((float) 0.6, 1) * 100));
	}

}
