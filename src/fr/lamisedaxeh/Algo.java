package fr.lamisedaxeh;

import java.util.ArrayList;

public class Algo {

	/**
	 * Calcul l'activation avec la sigmoïde à coef -2
	 * @param value
	 * @return
	 */
	private static double activation(double value) {
		return 1 / (1 + Math.exp(-2 * value));
	}

	public static void apprentisage(Neurone n, ArrayList<ArrayList<Double>> features, ArrayList<Double> target) {
		/*"""
        Method used to train the model using the gradient descent method
		 **input: **
		 *features: (Numpy Matrix)
		 *targets: (Numpy vector)
		 *weights: (Numpy vector)
		 *bias: (Integer)
		 **return (Numpy vector, Numpy vector) **
		 *update weights
		 *update bias
	    """
	    epochs = 100
	    learning_rate = 0.1

	    # Print current Accuracy
	      = predict(features, weights, bias)
	    print("Accuracy = %s" % np.mean(predictions == targets))

	    # Plot points
	    plt.scatter(features[:, 0], features[:, 1], s=40, c=targets, cmap=plt.cm.Spectral)
	    plt.show()

	    for epoch in range(epochs):
	        # Compute and display the cost every 10 epoch
	        if epoch % 10 == 0:
	            predictions = activation(pre_activation(features, weights, bias))
	            print("Current cost = %s" % cost(predictions, targets))
	        # Init gragients
	        weights_gradients = np.zeros(weights.shape)
	        bias_gradient = 0.
	        # Go through each row
	        for feature, target in zip(features, targets):
	            # Compute prediction
	            z = pre_activation(feature, weights, bias)
	            y = activation(z)
	            # Update gradients
	            weights_gradients += (y - target) * derivative_activation(z) * feature
	            bias_gradient += (y - target) * derivative_activation(z)
	        # Update variables
	        weights = weights - (learning_rate * weights_gradients)
	        bias = bias - (learning_rate * bias_gradient)
	    # Print current Accuracy
	    predictions = predict(features, weights, bias)
	    print("Accuracy = %s" % np.mean(predictions == targets))
		 */
		int epochs = 100;
		double learning_rate = 0.1;

		//		r.calculs();
		//		double tauxErreur = tauxErreur(r.getResult(), 1);

		for (int i = 0; i < epochs; i++) {
			if (i % 10 == 0) {
				System.out.println("Precision :" + precision(n, features, target));
			}
			//Init des variable pour calcul du gradients
			ArrayList<Double> weights_gradients = new ArrayList<Double>();
			weights_gradients.add(0.);
			weights_gradients.add(0.);
			double bias_gradient = 0.;

			//For each data
			for (int k = 0; k < features.size(); k++) {
				Double objectifPrediction = target.get(k);



				n.getEntree().get(0).setValue(features.get(k).get(0));
				n.getEntree().get(1).setValue(features.get(k).get(1));



				n.activationResult();
				double resultatCalcul = n.getValue();

				//MAJ des gradients
				//Calc le gradients de chaque arrete d'entrée
				for (Neurone ne: n.getEntree()) {
					bias_gradient += (resultatCalcul - objectifPrediction)
							* derivative_activation(n.getPre_activation());
				}
				weights_gradients.set(0, weights_gradients.get(0) + ((resultatCalcul - objectifPrediction)
						* derivative_activation(n.getPre_activation()) * n.getEntree().get(0).getValue()));
				weights_gradients.set(1, weights_gradients.get(1) + ((resultatCalcul - objectifPrediction)
						* derivative_activation(n.getPre_activation()) * n.getEntree().get(1).getValue()));
			}
			for (int j = 0; j < weights_gradients.size(); j++) {
				weights_gradients.set(j, n.getPoids().get(j) - (learning_rate * weights_gradients.get(j)));
			}
			n.setPoids(weights_gradients);
			n.setBias(n.getBias() - (learning_rate * bias_gradient));

			// Print current Accuracy
			//		    predictions = predict(features, weights, bias)
			//		    print("Accuracy = %s" % np.mean(predictions == targets))*/
		}

	}

	/**
	 * Calcul la dérivée de l'activation sigmoïde
	 *
	 * @param value
	 * @return
	 */
	private static double derivative_activation(double value) {
		return activation(value) * (1 - activation(value));
	}

	public static double precision(Neurone n, ArrayList<ArrayList<Double>> features, ArrayList<Double> target) {
		double result = 0;
		for (int k = 0; k < features.size(); k++) {
			Double objectifPrediction = target.get(k);

			n.getEntree().get(0).setValue(features.get(k).get(0));
			n.getEntree().get(1).setValue(features.get(k).get(1));

			n.activationResult();
			double resultatCalcul = n.getValue();
			// System.out.println(resultatCalcul);
			if (Math.round(resultatCalcul) == objectifPrediction) {
				result++;
			}

		}
		return result / features.size();
	}

	/**
	 * Calcul le taux d'erreur
	 *
	 * @param valeurObtenue  valeurObtenue lors de la prédiction
	 * @param valeurSouhaite valeur Souhaité
	 * @return taux d'erreur
	 */
	public static double tauxErreur(double valeurObtenue, double valeurSouhaite) {
		return (valeurSouhaite - valeurObtenue) * (valeurSouhaite - valeurObtenue);
	}

	/**
	 * Calcul l'urgence à augmenter P
	 *
	 * @param valeurObtenue  valeurObtenue lors de la prédiction
	 * @param valeurSouhaite valeur Souhaité
	 * @return urgence à augmenter P
	 */
	public static double urgenceAAugmenterP(double valeurObtenue, double valeurSouhaite) {
		return 2 * (valeurSouhaite - valeurObtenue);
	}

}
