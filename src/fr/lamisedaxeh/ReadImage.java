package fr.lamisedaxeh;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class ReadImage {
	private BufferedImage img = null;

	private File f = null;

	/**
	 * Initialiser une nouvelle image
	 *
	 * @param pathImg chemin vers l'image
	 * @throws InvalidAlgorithmParameterException Exeption si l'image en fait pas
	 *                                            384x384
	 */
	public ReadImage(String pathImg) throws InvalidAlgorithmParameterException {

		// read image
		try {
			f = new File(pathImg);
			img = ImageIO.read(f);
			int width = img.getWidth();
			int height = img.getHeight();
			if ((width != 50) && (height != 50)) {
				throw new InvalidAlgorithmParameterException();
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	/**
	 * Retourne une ArrayList de double avec les valeurs de chaque pixel entre 0 et
	 * 1
	 *
	 * @return ArrayList de double avec les valeurs de chaque pixel entre 0 et 1
	 */
	public ArrayList<Double> getArray() {
		ArrayList<Double> result = new ArrayList<Double>();
		for (int x = 0; x < 50; x++) {
			for (int y = 0; y < 50; y++) {
				result.add(getOnePixel(x, y) / 765.0);
			}
		}
		return result;
	}

	/**
	 * Retourne la somme de RGB
	 *
	 * @param x cordonée x du pixel
	 * @param y cordonée x du pixel
	 * @return la somme des couleurs RGB sur 765
	 */
	public int getOnePixel(int x, int y) {
		int p = img.getRGB(0, 0);
		// get red
		int r = (p >> 16) & 0xff;
		// get green
		int g = (p >> 8) & 0xff;
		// get blue
		int b = p & 0xff;

		return r + g + b;

	}

}
