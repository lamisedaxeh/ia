package fr.lamisedaxeh;

import java.security.InvalidAlgorithmParameterException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Couche {
	private final List<Neurone> neurones = new LinkedList<Neurone>();

	public Couche() {
	}

	public Couche(int nbNeurones) {
		ajoutDeNeurones(nbNeurones);
	}

	/**
	 * Ajoute et setup de plusieur neurones
	 *
	 * @param nbNeurones
	 */
	public void ajoutDeNeurones(int nbNeurones) {
		for (int i = 0; i < nbNeurones; i++) {
			neurones.add(new Neurone());
		}
	}

	/**
	 * calcul des résultats pour chaque neurones de la couche
	 */
	public void calculs() {
		for (Neurone neurone : neurones) {
			neurone.activationResult();
		}
	}

	/**
	 * @return the neurones
	 */
	public List<Neurone> getNeurones() {
		return neurones;
	}

	public void linkEntrées(Couche coucheEntrée) {
		Random ra = new Random();
		linkEntrées(coucheEntrée, Math.random());
	}

	/**
	 * Connecte les entrées avec les neurones de coucheEntrée
	 *
	 * @param coucheEntrée
	 */
	public void linkEntrées(Couche coucheEntrée, double poids) {
		for (Neurone neurone : neurones) {
			for (Neurone neurone2 : coucheEntrée.getNeurones()) {
				neurone.addEntrée(neurone2, (float) poids);
			}
		}
	}

	/**
	 * Set la valeurs de chaque neurone avec la valeurs des pixel de l'image
	 *
	 * @param image lien vers l'image
	 * @throws InvalidAlgorithmParameterException en cas de probléme avec le lien de
	 *                                            l'image ou de la taille de l'image
	 */
	public void setInput(String imageLink) throws InvalidAlgorithmParameterException {
		ReadImage image;
		image = new ReadImage(imageLink);
		ArrayList<Double> pixel = image.getArray();
		if (pixel.size() != neurones.size()) {
			throw new InvalidAlgorithmParameterException();
		}
		for (Neurone neurone : neurones) {
			neurone.setValue(pixel.remove(0));
		}
	}
}
