package fr.lamisedaxeh;

import java.util.LinkedList;
import java.util.List;

public class Neurone {
	/**
	 * Liste neurone de sortie
	 */
	public final List<Neurone> sortie = new LinkedList<Neurone>();
	/**
	 * Liste de neurone en entrées
	 */
	private final List<Neurone> entree = new LinkedList<Neurone>();

	/**
	 * Poids des arrêtes d'entrée
	 */
	private List<Double> poids = new LinkedList<Double>();

	private Double bias = Math.random();

	private double value = 0;
	private double pre_activation = 0;

	public Neurone() {
	}

	/**
	 * Init un neurone avec uniquement une valeur
	 *
	 * @param value valeur du setup
	 */
	public Neurone(double value) {
		this.value = value;
	}

	/**
	 * Calcul le résultat du neurone Avec la fonction d'activation sigmoïde
	 */
	public double activationResult() {
		calcul();
		value = 1 / (1 + Math.exp(-1 * pre_activation));
		return value;
	}

	/**
	 * Ajout d'un neurone en entrée
	 *
	 * @param n Le neurone à ajouter
	 * @param p Le poids du lien entre les neurones
	 */
	public void addEntrée(Neurone n, double p) {
		entree.add(n);
		n.sortie.add(this);
		poids.add(p);
	}

	private void calcul() {
		pre_activation = bias;
		for (int i = 0; i < entree.size(); i++) {
			pre_activation += entree.get(i).value * poids.get(i);
		}
	}

	/**
	 * @return the bias
	 */
	public Double getBias() {
		return bias;
	}

	/**
	 * @return the entree
	 */
	public List<Neurone> getEntree() {
		return entree;
	}

	/**
	 * @return the poids
	 */
	public List<Double> getPoids() {
		return poids;
	}


	/**
	 * @return the pre_activation
	 */
	public double getPre_activation() {
		return pre_activation;
	}

	/**
	 * @return the sortie
	 */
	public List<Neurone> getSortie() {
		return sortie;
	}

	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}

	/**
	 * Initialise le dernier neurone
	 *
	 * @param ce Couche d'entrée
	 */
	public void initineuroneDernier(Couche ce) {
		sortie.addAll(ce.getNeurones());
		for (Neurone element : sortie) {
			poids.add((double) 1);
		}

	}

	/**
	 * Initialise le premier neurone
	 *
	 * @param ce Couche d'entrée
	 */
	public void initineuronePremier(Couche ce) {
		entree.addAll(ce.getNeurones());
		for (Neurone element : entree) {
			poids.add((double) 1);
		}
	}

	/**
	 * Ajoute les neurones de la couche entree et sortie
	 *
	 * @param ce Couche d'entrée
	 * @param cs Couche de sortie
	 */
	public void initNeurone(Couche ce, Couche cs) {
		entree.addAll(ce.getNeurones());
		sortie.addAll(cs.getNeurones());
		for (Neurone element : entree) {
			poids.add((double) 1);
		}
	}

	public void retropropagation() {

	}

	/**
	 * @param bias the bias to set
	 */
	public void setBias(Double bias) {
		this.bias = bias;
	}

	/**
	 * @param value the value to set
	 */
	public void setPoids(List<Double> value) {
		poids = value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Neurone [sortie=" + sortie + ", entree=" + entree + ", poids=" + poids + ", bias=" + bias + ", value="
				+ value + "]";
	}

}