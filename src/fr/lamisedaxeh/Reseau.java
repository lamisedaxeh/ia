package fr.lamisedaxeh;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Reseau {
	/**
	 * Calcul l'activation avec la sigmoïde à coef -2
	 * @param value
	 * @return
	 */
	private static double activation(double value) {
		return 1 / (1 + Math.exp(-2 * value));
	}

	/**
	 * Calcul la dérivée de l'activation sigmoïde
	 *
	 * @param value
	 * @return
	 */
	private static double derivative_activation(double value) {
		return activation(value) * (1 - activation(value));
	}

	private final List<Couche> couches = new LinkedList<Couche>();

	public Reseau() {
	}

	public void addCouche(int nbNeurone) {
		Couche c = new Couche(nbNeurone);
		couches.add(c);
	}

	/**
	 * Entraine le réseau avec le jeu de donnée features
	 *
	 * @param n
	 * @param features jeu de donnée pour l'entrainement
	 * @param targets
	 */
	public void apprentisage(ArrayList<ArrayList<Double>> features, ArrayList<ArrayList<Double>> targets) {

		// Nombre de tours pour l'apprendtisage
		int epochs = 1000;
		double learning_rate = 1;

		// VERIF FEATURES
		if (features.get(0).size() != couches.get(0).getNeurones().size()) {
			throw new IllegalArgumentException();
		}

		for (int i = 0; i < epochs; i++) {
			// if (i % 10 == 0) {
			// Affiche la précission tous les 10 tours
			System.out.println("Tour apprentissage : " + i + " Precision :" + precision(features, targets));
			// }

			// Init des variables pour calcul du gradients
			ArrayList<ArrayList<ArrayList<Double>>> weights_gradients = new ArrayList<ArrayList<ArrayList<Double>>>();

			for (Couche couche2 : couches) {
				ArrayList<ArrayList<Double>> d = new ArrayList<ArrayList<Double>>();
				for (Neurone n : couche2.getNeurones()) {
					ArrayList<Double> d2 = new ArrayList<Double>();
					for (Double double1 : n.getPoids()) {
						d2.add(0.);
					}
					d.add(d2);
				}
				weights_gradients.add(d);
			}

			ArrayList<ArrayList<Double>> bias_gradients = new ArrayList<ArrayList<Double>>();
			// Ajout autemps de bias_gradient dans le tableau que dans le réseau
			for (Couche couche2 : couches) {
				ArrayList<Double> d = new ArrayList<Double>();
				for (Neurone element : couche2.getNeurones()) {
					d.add(0.);
				}
				bias_gradients.add(d);
			}


			// For each data
			for (int k = 0; k < features.size(); k++) {
				ArrayList<Double> objectifPrediction = targets.get(k);

				// Set les input du neurone avec les valeurs dans la features
				setInputReseau(features.get(k));

				calculs();
				ArrayList<Double> resultatCalcul = getResultReseau();

				// Boucle sur tous les neurones du réseau
				for (int couche = couches.size() - 1; couche >= 0; couche--) {
					for (int neurone = 0; neurone < couches.get(couche).getNeurones().size(); neurone++) {
						Neurone n = couches.get(couche).getNeurones().get(neurone);

						// MAJ des gradients
						// Calc le gradients du bias d'entré
						for (Neurone ne : n.getEntree()) {
							Double bias_gradient = tauxErreur(couche, neurone, objectifPrediction)
									* derivative_activation(n.getPre_activation());
							// on fait =+ dans le tableau la ou le bias gradients du neurone est
							// savegardé
							// neurone*couche
							bias_gradients.get(couche).set(neurone,
									bias_gradient + bias_gradients.get(couche).get(neurone));
						}

						// Calc le gradients de chaque arrete d'entrée
						for (int l = 0; l < n.getEntree().size(); l++) {
							Double weights_gradient = (tauxErreur(couche, neurone,
									objectifPrediction)
									* derivative_activation(n.getPre_activation())
									* n.getEntree().get(l).getValue());
							weights_gradients.get(couche).get(neurone).set(l,
									weights_gradients.get(couche).get(neurone).get(l)
									+ weights_gradient);
						}

					}
				}
			}


			for (int couche = couches.size() - 1; couche >= 0; couche--) {
				for (int neurone = 0; neurone < couches.get(couche).getNeurones().size(); neurone++) {
					Neurone n = couches.get(couche).getNeurones().get(neurone);
					for (int j = 0; j < weights_gradients.get(couche).get(neurone).size(); j++) {

						Double weights_gradient = n.getPoids().get(j)
								- (learning_rate * weights_gradients.get(couche).get(neurone).get(j));
						weights_gradients.get(couche).get(neurone).set(j, weights_gradient);
					}
					// MAJ du neurone
					n.setPoids(weights_gradients.get(couche).get(neurone));
					n.setBias(n.getBias() - (learning_rate * bias_gradients.get(couche).get(neurone)));
				}
			}

		}

		//		for (int c = couches.size() - 1; c >= 0; c--) {
		//			for (int neuroneIndex = 0; neuroneIndex < couches.get(c).getNeurones().size(); neuroneIndex++) {
		//				Neurone n = couches.get(c).getNeurones().get(neuroneIndex);
		//				if (n.getEntree().isEmpty()) { // ENTRAINEMENT POUR LA PREMIÉRE LIGNE
		//					return;
		//				} else if (c == couches.size()) { // ENTRAINEMENT POUR LA DERNIERE LIGNE
		//
		//					// Nombre de tours pour l'apprendtisage
		//					int epochs = 100;
		//					double learning_rate = 0.1;
		//
		//					// VERIF FEATURES
		//					if (features.get(0).size() != n.getEntree().size()) {
		//						throw new IllegalArgumentException();
		//					}
		//
		//					for (int i = 0; i < epochs; i++) {
		//
		//						if (i % 10 == 0) { // Affiche la précission tous les 10 tours
		//							System.out.println("Precision :" + precision(features, targets.get(neuroneIndex)));
		//						}
		//
		//						// Init des variable pour calcul du gradients
		//						ArrayList<Double> weights_gradients = new ArrayList<Double>();
		//						for (Double double1 : n.getPoids()) {
		//							weights_gradients.add(0.);
		//						}
		//
		//						double bias_gradient = 0.;
		//
		//						// For each data
		//						for (int k = 0; k < features.size(); k++) {
		//							Double objectifPrediction = targets.get(neuroneIndex).get(k);
		//
		//							// Set les input du neurone avec les valeurs dans la features
		//						for (int l = 0; l < n.getEntree().size(); l++) {
		//							n.getEntree().get(l).setValue(features.get(k).get(l));
		//							}
		//
		//							n.activationResult();
		//							double resultatCalcul = n.getValue();
		//
		//							// MAJ des gradients // Calc le gradients du bias d'entré
		//						for (Neurone ne : n.getEntree()) {
		//								bias_gradient += (resultatCalcul - objectifPrediction)
		//										* derivative_activation(n.getPre_activation());
		//							}
		//
		//							// Calc le gradients de chaque arrete d'entrée
		//						for (int l = 0; l < n.getEntree().size(); l++) {
		//							weights_gradients.set(l,
		//										weights_gradients.get(l) + ((resultatCalcul - objectifPrediction)
		//												* derivative_activation(n.getPre_activation())
		//												* n.getEntree().get(l).getValue()));
		//						}
		//						}
		//
		//						for (int j = 0; j < weights_gradients.size(); j++) {
		//							weights_gradients.set(j, n.getPoids().get(j) - (learning_rate * weights_gradients.get(j)));
		//						}
		//
		//						// MAJ du neurone
		//						n.setPoids(weights_gradients);
		//						n.setBias(n.getBias() - (learning_rate * bias_gradient));
		//					}
		//				} else { // ENTRAINEMENT POUR LES LIGNE DU MILLIEU
		//				}
		//			}
		//		}
		System.out.println(this.toString());

	}



	/**
	 * Calcul la sortie du réseau
	 */
	public void calculs() {
		for (int i = 1; i <= couches.size() - 1; i++) {
			couches.get(i).calculs();
		}
	}

	/**
	 * @return the couches
	 */
	public List<Couche> getCouches() {
		return couches;
	}

	/**
	 * Retourne la valeur du dernier noeud du réseau
	 */
	public double getResult() {
		return couches.get(couches.size() - 1).getNeurones().get(0).getValue();
	}

	/**
	 * Retourne les résultats du réseau
	 *
	 * @return La liste de valeurs de chaque neurone
	 */
	private ArrayList<Double> getResultReseau() {
		ArrayList<Double> result = new ArrayList<Double>();
		for (Neurone n : couches.get(couches.size() - 1).getNeurones()) {
			result.add(n.getValue());
		}
		return result;
	}

	/**
	 * Calcul la précission du réseau avec le jeu de donnée features
	 *
	 * @param n
	 * @param features jeu de donnée de test
	 * @param target   objectif resultat lors des test
	 * @return Ratio de bonne réponse 1 = 100% bonne réponse 0 = 0% bonne réponse
	 */
	public double precision(ArrayList<ArrayList<Double>> features, ArrayList<ArrayList<Double>> target) {
		double result = 0;
		for (int k = 0; k < features.size(); k++) {
			ArrayList<Double> objectifPrediction = target.get(k);

			setInputReseau(features.get(k));
			//			System.out
			//			.println(getCouches().get(0).getNeurones().get(0).getValue() + ","
			//					+ getCouches().get(0).getNeurones().get(0).getValue());
			calculs();
			ArrayList<Double> resultatCalcul = getResultReseau();
			// System.out.println(resultatCalcul);
			int flag = 0;
			for (int i = 0; i < resultatCalcul.size(); i++) {

				// System.out.println(getResult() + "==" + objectifPrediction.get(i));
				if (Math.round(resultatCalcul.get(i)) == objectifPrediction.get(i)) {
					flag++;
				}else
				{
					// System.out.println(resultatCalcul.get(i) + "==" + objectifPrediction.get(i));
				}
			}
			if (flag == objectifPrediction.size()) {
				result++;
			}

		}
		return result / (target.size());
	}

	/**
	 * Set les entrées du réseau avec les donnée dans dataInput
	 *
	 * @param dataInput La liste des donnée en entrées du réseau elle dois contenir
	 *                  autemps de valeur que d'entrée
	 */
	private void setInputReseau(ArrayList<Double> dataInput) {
		if (dataInput.size() != couches.get(0).getNeurones().size()) {
			throw new IllegalArgumentException();
		}
		for (int n = 0; n < couches.get(0).getNeurones().size(); n++) {
			couches.get(0).getNeurones().get(n).setValue(dataInput.get(n));
		}
	}

	/**
	 * Calcul le taux d'erreur
	 *
	 * @param valeurObtenue  valeurObtenue lors de la prédiction
	 * @param valeurSouhaite Les valeur Souhaité
	 * @return taux d'erreur
	 */
	public Double tauxErreur(int coucheDuNeurone, int hauteurDuNeurone, ArrayList<Double> valeurSouhaite) {
		//CASE OF LAST LINE
		Neurone neurone = couches.get(coucheDuNeurone).getNeurones().get(hauteurDuNeurone);
		if(coucheDuNeurone == couches.size()-1) {
			return (neurone.getValue() - valeurSouhaite.get(hauteurDuNeurone));
		}
		Double result = 0.;
		for (int i = 0; i < neurone.getSortie().size(); i++) {
			Neurone neuroneSortie = neurone.getSortie().get(i);
			result += tauxErreur(coucheDuNeurone + 1, i, valeurSouhaite)
					* neuroneSortie.getPoids().get(hauteurDuNeurone);
		}
		return result;

	}
}
